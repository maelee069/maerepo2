#!/bin/bash
yum update -y
yum install docker -y
service docker start
docker build -t mae-demo-img /opt/apps/.
sleep 3
docker run -itd --rm --name mae-demo-docker -p 8080:80 mae-demo-img

